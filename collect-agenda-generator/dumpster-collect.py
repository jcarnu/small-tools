
from datetime import date
from datetime import timedelta
from icalendar import Calendar
from icalendar import Event
from icalendar import Alarm

"""
Generates an icalfile in /tmp/cal.ics with
computed dates for different dumpster type
dates
"""
if __name__ == "__main__":
    td = timedelta(days=14)
    wk = timedelta(days=7)

    dates = {"déchets verts": [
                date(2023,  1,  4),
                date(2023,  1, 26),
                date(2023,  2, 15),
                date(2023,  3, 10),
                date(2023,  3, 30),
                date(2023,  4, 24),
                date(2023,  5, 12),
                date(2023,  6,  2),
                date(2023,  6, 22),
                date(2023,  7, 13),
                date(2023,  8,  2),
                date(2023,  8, 23),
                date(2023,  9, 13),
                date(2023, 10,  9),
                date(2023, 10, 27),
                date(2023, 11, 17),
                date(2023, 12,  7),
                date(2023, 12, 27),
            ],
            "recyclage": [date(2023, 1, 10)+i*td for i in range(1, 26)],
            "ménager": [date(2023, 1, 2) + wk * i for i in range(1, 52) if (date(2023, 1, 2) + wk * i) not in [date(2023, 5, 1), date(2023, 12, 24)]] + [date(2023, 4, 29), date(2023, 12, 23), date(2023, 12, 30)],
            "encombrants": [date(2023, 2, 22), date(2023, 9, 6)]
            }

    cal = Calendar()

    for name, dtz in dates.items():
        for dt in dtz:
            ev = Event()
            ev.add('dtstart', dt)
            ev.add('summary', name.capitalize())
            al = Alarm()
            al.add('trigger', timedelta(days=1))
            al.add('action', 'DISPLAY')
            al.add('description', name.capitalize())
            ev.add_component(al)
            cal.add_component(ev)

    with open("/tmp/cal.ics", "w") as x:
        x.write(cal.to_ical().decode("utf-8"))
